package git.browse;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Vector;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class StatsPLM {

	static Map<String,Map<String,Vector<String>>> diffs = new HashMap<String,Map<String,Vector<String>>>();

	public static void scanFiles() {
		File dir = new File("logsDir");
		File[] directories = dir.listFiles();
		File[] files;
		for(int i = 0 ; i<directories.length ; i++) {
			String exoID = directories[i].getName();
			files = directories[i].listFiles();
			for(int j = 0 ; j<files.length ; j++) {
				String id = files[j].getName();
				String filePath = "logsDir/"+exoID+"/"+files[j].getName();
				Scanner scanner = null;
				try {
					scanner = new Scanner(new File(filePath));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				String error = "";
				while (scanner.hasNextLine()) {
					error = error + scanner.nextLine();
				}
				if(diffs.get(exoID) == null) {
					diffs.put(exoID, new HashMap<String,Vector<String>>());
				}
				if(diffs.get(exoID).get(error) == null) {
					diffs.get(exoID).put(error, new Vector<String>());
					diffs.get(exoID).get(error).add(id);
				}
				else {
					if(!diffs.get(exoID).get(error).contains(id)) {
						diffs.get(exoID).get(error).add(id);
					}
				}
			}
		}
	}

	public static void writeWorkSheet(boolean xls) throws InterruptedException {
		Workbook wb;
		Object sheet;
		Row row;
		if(!xls) {
			wb = new XSSFWorkbook();
			sheet = (XSSFSheet) wb.createSheet("Result");

			//XSSFRow row;
		} else {
			wb = new HSSFWorkbook();
			sheet = (HSSFSheet) wb.createSheet("Result");

			//HSSFRow row;
		}
		int i = 0;
		int j = i;
		for(String exoID : diffs.keySet()) {
			//System.out.println("1 : i="+i+";j="+j);
			if(!xls) {
				row = ((XSSFSheet) sheet).createRow(i);
			} else {
				row = ((HSSFSheet) sheet).createRow(i);
			}
			i++;
			row.createCell((short)0, XSSFCell.CELL_TYPE_STRING).setCellValue(exoID);
			j = i-1;
			Thread.sleep(10);
			for(String error : diffs.get(exoID).keySet()) {
				//System.out.println("2 : i="+i+";j="+j);
				Row subRow;
				if(!xls) {
					subRow = (XSSFRow) row;
				} else {
					subRow = (HSSFRow) row;
				}
				if(i-1!=j) {
					if(!xls) {
						subRow = ((XSSFSheet) sheet).createRow(j);
					} else {
						subRow = ((HSSFSheet) sheet).createRow(j);
					}
				}
				j++;
				if(error!=null) {
					//if(!xls) {
					if(error.length()>30000) {
						subRow.createCell((short)1, XSSFCell.CELL_TYPE_STRING).setCellValue(error.substring(0, 32766));
					} else {
						subRow.createCell((short)1, XSSFCell.CELL_TYPE_STRING).setCellValue(error);
					}
					/*} else {
						if(error.length()>30000) {
							subRow.createCell((short)1, XSSFCell.CELL_TYPE_STRING).setCellValue(error.substring(0,29999));
						} else {
							subRow.createCell((short)1, XSSFCell.CELL_TYPE_STRING).setCellValue(error);
						}
					}*/
					subRow.createCell((short)2).setCellValue(diffs.get(exoID).get(error).size());
				}
				Thread.sleep(10);
			}
			//System.out.println("3 : i="+i+";j="+j);
			i=j;
		}

		FileOutputStream fileOut;
		try {
			if(!xls) {
				fileOut = new FileOutputStream("result.xlsx");
			} else {
				fileOut = new FileOutputStream("result.xls");
			}
			wb.write(fileOut);
			fileOut.close();
			wb.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/*WritableWorkbook workbook = null;
		try {
			workbook = Workbook.createWorkbook(new File("result.xls"));
			WritableSheet sheet = workbook.createSheet("Result", 0); 
			int y = 0;
			for(String exoID : diffs.keySet()) {
				sheet.addCell(new Label(0, y, exoID));
				for(String error : diffs.get(exoID).keySet()) {
					sheet.addCell(new Label(1, y, error));
					sheet.addCell(new Number(2, y, diffs.get(exoID).get(error).size()));
					y++;
					Thread.sleep(50);
					workbook.
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (RowsExceededException e) {
			e.printStackTrace();
		} catch (WriteException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		try {
			workbook.write();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			workbook.close();
		} catch (WriteException | IOException e) {
			e.printStackTrace();
		}*/
	}

	public static void main(String[] args) throws InterruptedException {
		boolean xls = false;
		scanFiles();
		writeWorkSheet(xls);
	}

}
