(require 'ox-latex)
(unless (boundp 'org-latex-classes)
  (setq org-latex-classes nil))
(add-to-list 'org-latex-classes
             '("tnreport"
               "\\documentclass{report}
	       \\usepackage{graphicx}
	       \\usepackage{amsfonts,amssymb,amsmath,mathtools}
	       \\usepackage[utf8]{inputenc}
	       \\usepackage[T1]{fontenc}
	       \\usepackage{libertine}
	       \\usepackage[libertine]{newtxmath}
	       \\usepackage[a4paper]{geometry}
	       \\geometry{top=2cm, bottom=2cm,right=2cm,left=2.5cm}
	       \\usepackage[colorlinks,linkcolor=black,citecolor=black,pagebackref]{hyperref}
	       \\usepackage{acronym}

	       \\usepackage{etoolbox}

	       \\usepackage{authblk}

	       \\usepackage{xcolor}
	       \\usepackage{titlesec}

	       \\usepackage[chapter]{tocbibind}
\\usepackage{glossaries}

\\usepackage{rotating}

	       \\usepackage[]{babel}

	       [NO-DEFAULT-PACKAGES]
	       [NO-PACKAGES]"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ))
