package git.browse;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

import javax.swing.JFrame;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import jxl.write.biff.RowsExceededException;
import models.GitEvent;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.ListBranchCommand.ListMode;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;
import org.eclipse.jgit.lib.Ref;

import plm.core.lang.ProgrammingLanguage;
import plm.core.model.Game;
import plm.core.model.lesson.Exercise;
import plm.core.model.lesson.Exercise.WorldKind;
import plm.universe.World;
import utils.GitUtils;

/* TODO:
 * 
 * - meilleur feedback
 * - temps de résolution
 * - nb d'essais maximal
 * - taux de complétion des leçons
 * - erreur de compil courante
 * - qté de travail en dehors de heures
 * - travail linéaire vs papillon / exercices papillonés
 * - qté de ragequit
 */

public class HarvesterPIDR {
	static int totalCommits = 0;	
	static List<Student> students = new ArrayList<Student>();
	static Map<String,Integer> scalaError = new HashMap<String,Integer>();
	static Map<String,Map<String,Vector<String>>> diffs = new HashMap<String,Map<String,Vector<String>>>();


	public static final ArrayList<String> lessonsName = new ArrayList<String>(Arrays.asList( // WARNING, keep ChooseLessonDialog.lessons synchronized
			"lessons.welcome", "lessons.turmites", "lessons.maze", "lessons.turtleart",
			"lessons.sort.basic", "lessons.sort.dutchflag", "lessons.sort.baseball", "lessons.sort.pancake", 
			"lessons.recursion.cons", "lessons.recursion.lego", "lessons.recursion.hanoi",
			"lessons.lightbot", "lessons.bat.string1", "lessons.lander"
			));

	public static void parse() throws InvalidRemoteException, TransportException, GitAPIException, IOException {
		GitUtils.fetchRepo(false);

		Git git = new Git(GitUtils.repository);

		List<Ref> branches = git.branchList().setListMode(ListMode.REMOTE).call();

		int nbStudents=0;

		System.out.println("Parse "+branches.size()+" branches. This may take a few seconds.");
		for(Ref branch:branches) {
			if(branch.getName().contains("PLM")) {				
				String branchName = branch.getName().substring(20);
				Student student = new Student(branchName);
				if (student.exoAttempted.size()>=1) {
					students.add(student);
					totalCommits += student.evtValidCount;
				}
				nbStudents++;
				if (nbStudents % 150 == 0)
					System.out.println(". "+nbStudents+"/"+branches.size());
				else
					System.out.print(".");

				for (String key:student.scalaError.keySet()) {
					if (!scalaError.containsKey(key))
						scalaError.put(key, student.scalaError.get(key));
					else
						scalaError.put(key, student.scalaError.get(key) + scalaError.get(key));
				}
			}
		}
		System.out.println();
	}
	static void printBranches() {
		System.out.println(Student.getHeader());

		Collections.sort(students);
		for (Student s:students)
			if (!s.usesBeta)
				System.out.println(s);		
	}
	static void printErrors() {
		for (String key:scalaError.keySet()) 
			System.out.println(scalaError.get(key)+": "+key);
		System.out.println("Scala errors: "+Student.handled+" handled; "+Student.unhandled+" unhandled.");		
	}
	static void printFeedback() {
		for (String exoName: Student.allClosedFeedback.keySet()) {

			if (Student.allClosedFeedback.get(exoName) != null && Student.allClosedFeedback.get(exoName).get("InterestNb") > 3) {
				System.out.print(exoName+": avg interest="+(Student.allClosedFeedback.get(exoName).get("InterestVal")/Student.allClosedFeedback.get(exoName).get("InterestNb")+" "));
				for (String val:Student.allClosedFeedback.get(exoName).keySet()) 
					System.out.print(val+"="+Student.allClosedFeedback.get(exoName).get(val)+";  ");
				System.out.println();
			}
		}		
	}
	static void printCumulative() {
		int[] passing = new int[201];
		int[] attempting = new int[201];
		int[] lines      = new int[4001];
		for (Student student: students) {
			if (! student.usesBeta) {
				for (int i=0;i<passing.length;i++) {
					if (student.exoPassed.size()>=i)
						passing[i] ++;
					if (student.exoAttempted.size()>=i)
						attempting[i] ++;
				}
				int passedLines = 0;
				for (String exoName : student.exoPassed.keySet())
					passedLines += student.exoPassed.get(exoName);
				for (int i=0;i<lines.length;i++) {
					if (passedLines >= i)
						lines[i]++;
				}
			}
		}

		System.out.println("# exo count , cumulative count of passing students, cumulative count of attempting students");
		for (int i=passing.length;i>=0;i--) {
			if ((i%25 == 0 && i!=0) || i==190 || i==10 || i==5 || i==1)
				System.out.format(" %4d ,  %4d,  %4d\n",i, passing[i], attempting[i]);
		}

		System.out.println("# lines count , cumulative count of students with more passing lines");
		for (int i=lines.length;i>=0;i--) {
			if ((i%1000==0 && i!=0) || i==500 || i==250 || i==50 || i==10)
				System.out.format("  %4d,  %4d\n",i, lines[i]);
		}
	}
	static void printMonthly() {
		Map<String,Integer> monthlyPassingUsers= new HashMap<String,Integer>(); 
		Map<String,Integer> monthlyJavaPassingUsers= new HashMap<String,Integer>(); 
		Map<String,Integer> monthlyScalaPassingUsers= new HashMap<String,Integer>(); 
		Map<String,Integer> monthlyPythonPassingUsers= new HashMap<String,Integer>(); 

		Map<String,Integer> monthlyPassedExo= new HashMap<String,Integer>(); 
		Map<String,Integer> monthlyJavaPassedExo= new HashMap<String,Integer>(); 
		Map<String,Integer> monthlyScalaPassedExo= new HashMap<String,Integer>(); 
		Map<String,Integer> monthlyPythonPassedExo= new HashMap<String,Integer>(); 

		for (Student student: students) {
			if (student.usesBeta)
				continue;
			for (String date:student.monthlyPassed.keySet()) {
				Student.incOrInitialize(monthlyPassingUsers, date);

				if (student.monthlyPassed.get(date).get("Java") != null) {
					Student.incOrInitialize(monthlyJavaPassingUsers, date);
					Student.addOrInitialize(monthlyJavaPassedExo, date, student.monthlyPassed.get(date).get("Java"));
					Student.addOrInitialize(monthlyPassedExo,     date, student.monthlyPassed.get(date).get("Java"));
				}
				if (student.monthlyPassed.get(date).get("Scala") != null) {
					Student.incOrInitialize(monthlyScalaPassingUsers, date);
					Student.addOrInitialize(monthlyScalaPassedExo, date, student.monthlyPassed.get(date).get("Scala"));
					Student.addOrInitialize(monthlyPassedExo,      date, student.monthlyPassed.get(date).get("Scala"));
				}
				if (student.monthlyPassed.get(date).get("Python") != null) {
					Student.incOrInitialize(monthlyPythonPassingUsers, date);
					Student.addOrInitialize(monthlyPythonPassedExo, date, student.monthlyPassed.get(date).get("Python"));
					Student.addOrInitialize(monthlyPassedExo,       date, student.monthlyPassed.get(date).get("Python"));
				}
			}
		}
		System.out.println("# Monthly statistics. For each column, we report passedExo/passingUsers that match the criteria");
		System.out.println("# Month, Monthly stats, Java only,   Python ,   Scala");

		System.out.println("| Month |exos_month|traces_month|exos_java_month|traces_java_month| exos_python_month|traces_python_month| exos_scala_month|traces_scala_month|");
		System.out.println("|-------+----------+------------+---------------+-----------------+------------------+-------------------+-----------------+------------------|");
		for (int year=2014; year<2016; year++) {
			for (int month=0; month<52; month++) {
				String date = ""+year+"."+month;
				Integer mu = monthlyPassingUsers.get(date);
				Integer me = monthlyPassedExo.get(date);
				if (me == null)
					me = 0;
				if (mu!=null) {
					int ju = monthlyJavaPassingUsers.get(date);
					int pu = monthlyPythonPassingUsers.get(date);
					int su = monthlyScalaPassingUsers.get(date);

					int je = monthlyJavaPassedExo.get(date);
					int pe = monthlyPythonPassedExo.get(date);
					int se = monthlyScalaPassedExo.get(date);
					System.out.format("15/%d/%d|  %5d|%4d  | %4d|%3d | %4d|%3d | %4d|%3d|\n",
							month,year, me,mu, je,ju, pe,pu, se,su);
				}
			}
		}

	}
	static void printDaily() {
		Map<String,Integer> dailyJavaPassingUsers= new HashMap<String,Integer>(); 
		Map<String,Integer> dailyScalaPassingUsers= new HashMap<String,Integer>(); 
		Map<String,Integer> dailyPythonPassingUsers= new HashMap<String,Integer>(); 

		Map<String,Integer> dailyJavaPassedExo= new HashMap<String,Integer>(); 
		Map<String,Integer> dailyScalaPassedExo= new HashMap<String,Integer>(); 
		Map<String,Integer> dailyPythonPassedExo= new HashMap<String,Integer>(); 

		for (Student student: students) {
			if (student.usesBeta)
				continue;
			for (String date:student.dailyPassedLang.keySet()) {

				if (student.dailyPassedLang.get(date).get("Java") != null) {
					Student.incOrInitialize(dailyJavaPassingUsers, date);
					Student.addOrInitialize(dailyJavaPassedExo, date, student.dailyPassedLang.get(date).get("Java"));
				}
				if (student.dailyPassedLang.get(date).get("Scala") != null) { 
					Student.incOrInitialize(dailyScalaPassingUsers, date);
					Student.addOrInitialize(dailyScalaPassedExo, date, student.dailyPassedLang.get(date).get("Scala"));
				}
				if (student.dailyPassedLang.get(date).get("Python") != null) { 
					Student.incOrInitialize(dailyPythonPassingUsers, date);
					Student.addOrInitialize(dailyPythonPassedExo, date, student.dailyPassedLang.get(date).get("Python"));
				}
			}
		}
		System.out.println("|Date|exos_java|traces_java| exos_python|traces_python| exos_scala|traces_scala|");
		System.out.println("|----+---------+-----------+------------+-------------+-----------+------------|");
		for (int year=2014; year<2016; year++) {
			for (int month=1; month<13; month ++) {
				for (int day=1; day<32; day ++) {
					String date = ""+year+"."+month+"."+day;
					Integer ju = intOrZero(dailyJavaPassingUsers.get(date));
					Integer je = intOrZero(dailyJavaPassedExo.get(date));
					Integer su = intOrZero(dailyScalaPassingUsers.get(date));
					Integer se = intOrZero(dailyScalaPassedExo.get(date));
					Integer pu = intOrZero(dailyPythonPassingUsers.get(date));
					Integer pe = intOrZero(dailyPythonPassedExo.get(date));
					if (0+ju+su+pu > 0) {	
						System.out.println("|"+date+"|"+je+"|"+ju+"|"+pe+"|"+pu+"|"+se+"|"+su+"|");
					}
				}
			}
		}
	}
	public static void printWeekly() {
		Map<String,Integer> dailyPassingUsers  = new HashMap<String,Integer>();
		Map<String,Integer> dailyPassedExo  = new HashMap<String,Integer>();

		Map<String,Integer> weeklyPassingUsers = new HashMap<String,Integer>();
		Map<String,Integer> weeklyPassedExo = new HashMap<String,Integer>();

		for (Student student: students) {
			if (student.usesBeta)
				continue;

			for (String date:student.dailyPassed.keySet())
				Student.incOrInitialize(dailyPassingUsers, date);
			for (String date:student.weeklyPassed.keySet())
				Student.incOrInitialize(weeklyPassingUsers, date);

			for (String date:student.dailyPassed.keySet())
				Student.addOrInitialize(dailyPassedExo, date, student.dailyPassed.get(date));
			for (String date:student.weeklyPassed.keySet())
				Student.addOrInitialize(weeklyPassedExo, date, student.weeklyPassed.get(date));
		}
		System.out.println("# Week,Weekly exos/studnts,  Monday ,  Tuesday,Wednesday, Thursday,  Friday , Saturday,   Sunday");

		String[] months = new String[] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
		Calendar cal = Calendar.getInstance();
		for (int year=2014; year<2017; year++) {
			for (int week=0; week<55; week ++) {
				Integer users = weeklyPassingUsers.get(""+year+"."+week);
				Integer exos = weeklyPassedExo.get(""+year+"."+week);
				if (users != null) {
					System.out.format("%d.%2d  ,   %4d/%3d     ",year,week+1,exos,users);
					cal.clear();
					cal.set(Calendar.YEAR, year);
					cal.set(Calendar.WEEK_OF_YEAR,week);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
					String dayDate = ""+year+"."+(1+cal.get(Calendar.MONTH))+"."+cal.get(Calendar.DAY_OF_MONTH);
					users = dailyPassingUsers.get(dayDate);
					exos  = dailyPassedExo.get(dayDate);
					if (users == null) users = 0;
					if (exos == null) exos = 0;
					System.out.format(", %4s/%3d",exos,users);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.TUESDAY);
					dayDate = ""+year+"."+(1+cal.get(Calendar.MONTH))+"."+cal.get(Calendar.DAY_OF_MONTH);
					users = dailyPassingUsers.get(dayDate);
					exos = dailyPassedExo.get(dayDate);
					if (users == null) users = 0;
					if (exos == null) exos = 0;
					System.out.format(", %4s/%3d",exos,users);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.WEDNESDAY);
					dayDate = ""+year+"."+(1+cal.get(Calendar.MONTH))+"."+cal.get(Calendar.DAY_OF_MONTH);
					users = dailyPassingUsers.get(dayDate);
					exos = dailyPassedExo.get(dayDate);
					if (users == null) users = 0;
					if (exos == null) exos = 0;
					System.out.format(", %4s/%3d",exos,users);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.THURSDAY);
					dayDate = ""+year+"."+(1+cal.get(Calendar.MONTH))+"."+cal.get(Calendar.DAY_OF_MONTH);
					users = dailyPassingUsers.get(dayDate);
					exos = dailyPassedExo.get(dayDate);
					if (users == null) users = 0;
					if (exos == null) exos = 0;
					System.out.format(", %4s/%3d",exos,users);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.FRIDAY);
					dayDate = ""+year+"."+(1+cal.get(Calendar.MONTH))+"."+cal.get(Calendar.DAY_OF_MONTH);
					users = dailyPassingUsers.get(dayDate);
					exos = dailyPassedExo.get(dayDate);
					if (users == null) users = 0;
					if (exos == null) exos = 0;
					System.out.format(", %4s/%3d",exos,users);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.SATURDAY);
					dayDate = ""+year+"."+(1+cal.get(Calendar.MONTH))+"."+cal.get(Calendar.DAY_OF_MONTH);
					users = dailyPassingUsers.get(dayDate);
					exos = dailyPassedExo.get(dayDate);
					if (users == null) users = 0;
					if (exos == null) exos = 0;
					System.out.format(", %4s/%3d",exos,users);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.SUNDAY);
					dayDate = ""+year+"."+(1+cal.get(Calendar.MONTH))+"."+cal.get(Calendar.DAY_OF_MONTH);
					users = dailyPassingUsers.get(dayDate);
					exos = dailyPassedExo.get(dayDate);
					if (users == null) users = 0;
					if (exos == null) exos = 0;
					System.out.format(", %4s/%3d",exos,users);

					cal.set(Calendar.DAY_OF_WEEK,Calendar.MONDAY);
					System.out.println("  # Week of monday "+ cal.get(Calendar.DAY_OF_MONTH)+ "th of "+months[cal.get(Calendar.MONTH)]);
				}
			}
		}
	}

	static int intOrZero(Integer i) {
		if (i==null)
			return 0;
		return i;
	}

	public static void main(String args[]) throws Exception {
		parse();
		
		System.setErr(new PrintStream(new OutputStream() {
		    public void write(int b) {
		    }
		}));

		/*printBranches();
		printErrors();
		printFeedback();

		printCumulative();
		printMonthly();
		printWeekly();
		printDaily();*/

		getCode();

		System.out.println("\nThere is "+ students.size() + " non-empty students (+ "+Student.betaUsers+" beta users), "+Student.passedExo+" passed exos (of which "+Student.feedback+" have a feedback) and "+ totalCommits + " valid commits!");
		System.out.println("Failed exos: "+Student.failed+"; compil error:"+Student.compil);
		
		System.out.println("=============== You can now stop the execution ===============");
	}
	
	public static void execCode(GitEvent commit, String branchName, String code, ProgrammingLanguage lang, String lessonID, String exoID) throws FileNotFoundException, InterruptedException {
		MockLogHandler log = new MockLogHandler();
		Game g = new Game(log, new JFrame().getLocale());
		g.getProgressSpyListeners().clear();
		g.removeSessionKit();
		g.setBatchExecution();
		
		g.setProgramingLanguage(lang);
		g.switchLesson(lessonID, true);
		g.getCurrentLesson().setCurrentExercise(exoID);
		g.setLocale(new Locale("en"));
		
		Exercise exo = (Exercise) g.getCurrentLesson().getCurrentExercise();
		exo.getSourceFile(lang, 0).setBody(code);
		g.startExerciseExecution();
		
		Vector<World> currentWorlds = exo.getWorlds(WorldKind.CURRENT);
		BufferedWriter bw = null;
		for(int i=0; i<currentWorlds.size(); i++) {
			World currentWorld = currentWorlds.get(i);
			World answerWorld = exo.getAnswerOfWorld(i);
			if (!currentWorld.winning(answerWorld)) {
				String error = answerWorld.diffTo(currentWorld);
				System.out.println(error);
				try {
					File f = new File("logsDir/"+exoID);
					f.mkdirs();
					File dest = new File("logsDir/"+exoID+"/"+branchName+"_"+commit.rev.name()+".log");
					dest.createNewFile();
					bw = new BufferedWriter(new FileWriter("logsDir/"+exoID+"/"+branchName+"_"+commit.rev.name()+".log", true));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					if(error!=null) {
						bw.write(error);
					} else {
						bw.write("null");
					}
					bw.flush();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				/*if(diffs.get(exoID) == null) {
					diffs.put(exoID, new HashMap<String,Vector<String>>());
				}
				if(diffs.get(exoID).get(error) == null) {
					diffs.get(exoID).put(error, new Vector<String>());
					diffs.get(exoID).get(error).add(branchName);
				}
				else {
					if(!diffs.get(exoID).get(error).contains(branchName)) {
						diffs.get(exoID).get(error).add(branchName);
					}
				}*/
			}
		}
		try {
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void getStats(PrintStream ps) { //deprecated
		WritableWorkbook workbook = null;
		try {
			workbook = Workbook.createWorkbook(new File("result.xls"));
			WritableSheet sheet = workbook.createSheet("Result", 0); 
			int y = 0;
			for(String exoID : diffs.keySet()) {
				sheet.addCell(new Label(0, y, exoID));
				for(String error : diffs.get(exoID).keySet()) {
					sheet.addCell(new Label(1, y, error));
					sheet.addCell(new Number(2, y, diffs.get(exoID).get(error).size()));
					y++;
					ps.println(exoID+" --> "+diffs.get(exoID).get(error).size());
					ps.println(error);
					ps.println("------------------------------------------------------------------------------------");
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (RowsExceededException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			workbook.write();
			workbook.close();
		} catch (WriteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static boolean rescanCache(String exoName, String branchName, String commitName) {
		File dir = new File("logsDir/"+exoName);
		File[] files = dir.listFiles();
		if(files==null)
			return false;
		for(int i = 0;i<files.length;i++) {
			//System.out.println(files[i].getName());
			if(files[i].getName().equals(branchName+"_"+commitName+".log")) {
				return true;
			}
		}
		return false;
	}

	@SuppressWarnings("deprecation")
	public static void getCode() throws IOException, GitAPIException, InterruptedException {
		PrintStream ps = new PrintStream("codes.txt");
		int ok = 0;
		
		for(Student student : students) {
			//Thread.sleep(2000);
			System.out.println("Entre dans la branche "+ok+" - "+student.name);
			if(ok<=8) {
				ps.println(ok + " --> " + student.name);
				ok++;
				ArrayList<GitEvent> commits = GitUtils.computeCommits(student.name.substring(3));
				Collections.sort(commits);
				int nbCo = 0;
				System.out.println("Nombre de commits dans la branche : "+commits.size());
				int nbThreadMax = 8;
				int nbThread = 1;
				for(int i = nbThreadMax;i>0;i--){
					if(commits.size()%i==0){
						nbThread = i;
						break;
					}
				}
				for(int i = 0;i<commits.size();i+=nbThread) {
					/*int l = 0;
					if((commits.size()-i)/nbThread==0) {
						l=nbThread+i-commits.size();
					}*/
					CommitThread[] cts = new CommitThread[nbThread];
					for(int j = 0;j<nbThread;j++) {
						cts[j] = new CommitThread(commits.get(i+j), ps, nbCo, student);
						//System.out.println("ok");
						if(rescanCache(commits.get(i+j).exoname, student.name, commits.get(i+j).rev.getName())) {
							//System.out.println("Déjà là !");
							continue;
						}
						try {
						cts[j].run();
						}
						catch(IndexOutOfBoundsException e) {
							System.out.println("plop "+j+" - "+cts.length);
							System.out.println("-->" + commits.get(i+j).rev.getName());
						}
					}
					for(int j = 0;j<nbThread;j++) {
						synchronized(cts[j]) {
							if(cts[j].isAlive())	
								cts[j].wait(30000);
							if(cts[j].isAlive()) {
								cts[j].stop();
							}
						}
					}
				}
			} else {
				break;
			}
		}
		//getStats(ps);
		ps.close();
		
//		String path = "repo/.git";
		//Repository repo = new FileRepository(path);
//		FileRepositoryBuilder builder = new FileRepositoryBuilder();
//		Repository repo = builder.setGitDir(new File(path)).readEnvironment().findGitDir().build();
//		Git git = new Git(repo);
		/*List<Ref> branches = git.branchList().call();

	    for (Ref branch : branches) {
	        String branchName = branch.getName();

	        System.out.println("Commits of branch: " + branch.getName());
	        System.out.println("-------------------------------------");

	        Iterable<RevCommit> commits = git.log().all().call();

	        for (RevCommit commit : commits) {
	            boolean foundInThisBranch = false;
	            RevWalk walk = new RevWalk(repo);
	            RevCommit targetCommit = walk.parseCommit(repo.resolve(
	                    commit.getName()));
	            for (Map.Entry<String, Ref> e : repo.getAllRefs().entrySet()) {
	                if (e.getKey().startsWith(Constants.R_HEADS)) {
	                    if (walk.isMergedInto(targetCommit, walk.parseCommit(
	                            e.getValue().getObjectId()))) {
	                        String foundInBranch = e.getValue().getName();
	                        if (branchName.equals(foundInBranch)) {
	                            foundInThisBranch = true;
	                            break;
	                        }
	                    }
	                }
	            }

	            if (foundInThisBranch) {
	                System.out.println(commit.getName());
	                System.out.println(commit.getAuthorIdent().getName());
	                System.out.println(new Date(commit.getCommitTime()));
	                System.out.println(commit.getFullMessage());
	            }
	        }
	    }*/
//		ArrayList<RevCommit> log = Lists.newArrayList(git.log().all().call());
//		HashMap<String, ArrayList<RevCommit>> hashLog = new HashMap<String, ArrayList<RevCommit>>();
//		System.out.println(getFromBranch(log.get(0), git));
//		//Collections.sort(log);
//		//for(Student s : students) {
//		//ps.println(s.name);
//		for(RevCommit revCommit : log) {
//			if(ok>=50) break;
//			System.out.println(ok);
//			ok++;
//			RevTree tree = revCommit.getTree();
//
//			TreeWalk treeWalk = new TreeWalk(repo);
//			treeWalk.addTree(tree);
//			treeWalk.setRecursive(true);
//			ObjectId objectId = null;
//			if(treeWalk.getDepth()!=0) 
//				objectId = treeWalk.getObjectId(0);
//			System.out.println(objectId);
//			if(objectId!=null) {
//				ObjectLoader loader = repo.open(objectId);
//				loader.copyTo(ps);
//			}
//			ps.println(revCommit.name());
//			ps.println();
//			ps.println("----------");
//		}
		//}
//		ps.println("===============================================================");
//		ok=0;
//		System.out.println("-----");
		/*RevWalk walk = new RevWalk(repo);
		for(Student s : students) {
			walk.markStart(walk.parseCommit(repo.resolve(repo.getBranch())));
			ps.println(s.name);
			for(Iterator<RevCommit> iterator = walk.iterator(); iterator.hasNext();) {
				if(ok>=50) break;
				System.out.println(ok);
				ok++;
				RevCommit revCommit = iterator.next();
				RevTree tree = revCommit.getTree();

				TreeWalk treeWalk = new TreeWalk(repo);
				treeWalk.addTree(tree);
				treeWalk.setRecursive(true);

				ObjectId objectId = treeWalk.getObjectId(0);
				ObjectLoader loader = repo.open(objectId);
				ps.println(revCommit.name());
				loader.copyTo(ps);
				ps.println();
				ps.println("----------");
			}
		}
		repo.close();
		//ObjectId lastCommitId = repo.resolve(Constants.HEAD);
		/*RevWalk revWalk = new RevWalk(repo);
		//RevCommit commit = revWalk.parseCommit(lastCommitId);
		ArrayList<RevCommit> revCommits = new ArrayList<RevCommit>();
		/*for(Student student : students) {
			if(student.usesBeta) {
				continue;
			}
			if(ok<=10) {
				String branchName = student.name;
				ArrayList<GitEvent> commits = GitUtils.computeCommits(branchName.substring(3));
				for(GitEvent commit:commits) {
					if(commit.isValid() && commit.evt_type.equalsIgnoreCase("Failed")) {
						revCommits.add(commit.rev);
					}
				}
				int nbCommits = 0;
				for (RevCommit commit: revCommits) {
					RevCommit revCommit = revWalk.parseCommit(commit.toObjectId());
					System.out.println(commit.toObjectId());
					RevTree tree = revCommit.getTree();

					TreeWalk treeWalk = new TreeWalk(repo);
					treeWalk.addTree(tree);
					treeWalk.setRecursive(true);

					ObjectId objectId = treeWalk.getObjectId(0);
					ObjectLoader loader = repo.open(objectId);
					ps.println(branchName);
					ps.println(commit.name());
					loader.copyTo(ps);
					//byte[] code = loader.getBytes();
					//System.out.println(code+" "+" : "+" ---> "+commit.getParentCount());
					ps.println();
					ps.println("----------");
					/*if(revCommit.getParentCount()!=0) {
						for(RevCommit parents : revCommit.getParents()) {
							tree = parents.getTree();

							treeWalk = new TreeWalk(repo);
							treeWalk.addTree(tree);
							treeWalk.setRecursive(true);

							objectId = treeWalk.getObjectId(0);
							loader = repo.open(objectId);
							loader.copyTo(ps);
							//byte[] code = loader.getBytes();
							//System.out.println(code+" "+" : "+" ---> "+commit.getParentCount());
							ps.println();
							ps.println("----------");
							nbCommits++;
						}
					}
				}
				System.out.println(branchName + " --> " + nbCommits);
				ok++;
			}
		}*/
		//revWalk.dispose();
		//repo.close();
	}
}